# The Magdeburg Groceries Dataset

The Magdeburg Groceries Dataset consists of 65.315 images from the retail domain. We split the dataset into two parts. 23.360 images were collected from the web and typically depict a single item under studio conditions. 41.955 images were generated with DGen[1], a tool designed to record data for image recognition approaches. The core idea behind the tool is to annotate objects in 3D and project their bounding box onto video sequences. Further, we were able to refine these objects with fine-grained grocery item annotations. All annotations were acquired as bounding boxes in the [PASCAL VOC][pascalvoc] format.

The Magdeburg Groceries is intended for academic research and non-commercial use only. If you use the data, please cite our work [1].

If there are any questions about this dataset, please contact:

    marco.filax (at) ovgu.de

## Introductory Video

 [![Introductory Video](http://img.youtube.com/vi/fgxYxVYHWlI/maxresdefault.jpg)](https://www.youtube.com/watch?v=fgxYxVYHWlI)

## Dataset
We split the dataset into two parts: i) categorized training images of groceries in a studio setting and ii) annotated frames of real-world shelves from three different grocery stores to reflect an industrial problem setting.

All images and annotations are available [here][data].

### Training Images
| ![10.1.4.0.180](img/180.jpg "10.1.4.0.180") | ![7.5.1.0.9335](img/9355.jpg "7.5.1.0.9335") | ![9.3.3.2.13517](img/13517.jpg "9.3.3.2.13517") |
|---|---|---|
| ![4.2.1.1.491](img/5491.jpg "4.2.1.1.5491") | ![4.2.2.3.5590](img/5590.jpg "4.2.2.3.5590") | ![4.3.1.2.5662](img/5662.jpg "4.3.1.2.5662") |

As training images, we automatically collect the set of items from the web. Exemplary images are shown above. All items have a resolution of 220x220 pixels. In total, we provide 23,360 images as *annotated web links*. We organized them in categories with a populated semantic hierarchy to reflect real-world product categories in typical grocery stores. Categories are linked with ”is-a” relations. We provide 942 categories in total with 24.8 items on average. The categorization is expressed as a hierarchical ID for every image:

    Category.Subcategory1.Subcategory2.Subcategory3.UniqueIdentifier.jpg

There exist 19 different top-level categories. We chose to incorporate a structured approach to denote the hierarchy and require every item to be stored in exactly three subcategories. If a subcategory does not own any children but does not provide a depth of three, we added empty subcategories to fit the criterion. Note that every category and subcategory is expressed as a category-wise unique number. Thus, the hierarchical categorization exemplary denoted as 4.3.1.2.5662 (cf. nuts in the upper figure). The ID 5662 represents the unique identifier of this item. The ID is unique for the complete dataset, which allows us to discard the hierarchy if it is not needed for the given purpose.

### Validation Images

| ![Example](img/scene1.png) | ![Example](img/scene2.jpg) | ![Example](img/scene3.jpg) |
|---|---|---|
| ![Example](img/scene4.jpg) | ![Example](img/scene5.jpg) | ![Example](img/scene6.jpg) |

As validation images, we recorded 48 video sequences in three different grocery stores with 953 frames on average. We spent approximately four hours per store. Every video sequence was sampled in a resolution of 3840x2160 pixels. Examples are shown in the figure above. We provide two different types of annotations: general recognition of shelves and fine-grained annotations of groceries.

#### Shelf Annotations

In total, we annotated 83 real-world shelves and recorded video sequences. We sampled the position of the shelves, the trajectory of the camera, and the sequence itself. We aimed at mimicking natural behavior and did not restrict viewpoint or illumination. Our dataset comprises 41,955 images annotated with challenging viewpoints and illumination changes. Annotations are provided as bounding boxes in the PASCAL VOC format. Every bounding box is annotated with a hierarchical identifier. These hierarchical IDs are chosen as specific as possible.

#### Grocery Annotations

Through methods described in [1], we were able to annotate a subset of these sequences offline with fine-grained annotations. Our dataset comprises 1,523 fine-grained item annotations in ten sequences in 17 shelves. Extracting these annotations generates 755,309 bounding boxes for 12,768 images in total. Annotations were done in roughly nine hours in total. We provide bounding boxes in the PASCAL VOC format. Every bounding box is annotated with a single unique identifier.

## Acknowledgments

We want to thank the *real GmbH* for giving us the opportunity to record this dataset.

## License

![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/)

## References

[1] Filax M., Gonschorek T., Ortmeier F. (2019): Data for Image Recognition Tasks: An Efficient Tool for Fine-Grained Annotations. In Proceedings of the 8th International Conference on Pattern Recognition Applications and Methods

[data]: http://cse.iks.cs.ovgu.de/downloads/groceries/md_groceries.zip
[pascalvoc]: http://host.robots.ox.ac.uk/pascal/VOC/
